package luv2code.com.cruddemo.dao;

import luv2code.com.cruddemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Integer> {

    //that is it ... no need to write any code LOL
}
