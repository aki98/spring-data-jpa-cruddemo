package luv2code.com.cruddemo.rest;

import luv2code.com.cruddemo.entity.Employee;
import luv2code.com.cruddemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {

    private EmployeeService employeeService;
     //adding
    //quick and dirty: inject employee dao
    @Autowired
    public EmployeeRestController(EmployeeService theEmployeeService){

        employeeService=theEmployeeService;
    }
    //expose "/employee" and return list of employee

    @GetMapping("/employees")
    public List<Employee> findAll(){
        return employeeService.findAll();
    }

    @GetMapping("/employees/{employeeId}")
    public Employee getEmployee(@PathVariable int employeeId)
    {
        Employee theEmployee = employeeService.findById( employeeId );
        if(theEmployee ==null){
            throw new RuntimeException( "Employee id not found "+employeeId);

        }

        return theEmployee;
    }

    @PostMapping("/employees")
    public Employee addEmployee(@RequestBody Employee theEmployee){

        theEmployee.setId( 0 );
        employeeService.save( theEmployee );

        return theEmployee;

    }

    @PutMapping("/employees")
    public Employee updateEmployee(@RequestBody Employee employee){
        employeeService.save( employee );
        return employee;
    }


    @DeleteMapping("/employees/{employeeId}")
    public String deleteEmployee(@PathVariable int employeeId){

        Employee employee = employeeService.findById( employeeId );
        if(employee == null){
            throw new RuntimeException( "Customer id is not found "+employeeId );
        }
        employeeService.deleteById( employeeId );
        return "Deleted customer id: "+employeeId;

    }

}
